'''
Created on Jun 25, 2013

@author: neverendingqs
'''
import unittest
from externalsort import externalsort

class externalsortTests(unittest.TestCase):
	
	def testSmallList(self):
		unsortedlist = [3, 5, 2, 7, 1]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 10))
		pass

	def testSmallListWithNegatives(self):
		unsortedlist = [4, -5, 2, -7, 1]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 7))
		pass

	def testExactSizeList(self):
		unsortedlist = [4, 23, 9, 11, 50, 1]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 6))
		pass
	
	def testExactSizeListWithNegatives(self):
		unsortedlist = [4, -95, 24, -76, 12, -5, 2]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 7))
		pass
	
	def testLargeList(self):
		unsortedlist = [3, 5, 2, 7, 1]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 3))
		pass
	
	def testLargeListWithNegatives(self):
		unsortedlist = [34, 432, -5, 2, -2, 87, -8767, 1, 432432, 90343]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 4))
		pass
	
	def testLargeListWithMultipleLevels(self):
		unsortedlist = [34, 432, -5, 2, -2, 87, -8767, 1, 432432, 90343]
		sortedlist = sorted(unsortedlist)
		self.assertEqual(sortedlist, externalsort.mergesort(unsortedlist, 2))
		pass

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()