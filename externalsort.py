'''
Created on Jun 25, 2013

@author: neverendingqs
'''

class externalsort(object):

	# def __init__(selfparams):

	@staticmethod
	def mergesort(unsortedblocks, memsize):
		
		# Memsize must be 2 or greater
		if memsize < 2:
			return []
				
		'''Base case: if we have a small block of integers, we can lazily sort'''
		if isinstance(unsortedblocks[0], int) and len(unsortedblocks) <= memsize:
			return sorted(unsortedblocks)
		
		'''If we have too many items, we must break this problem into smaller problems'''
		if len(unsortedblocks) > memsize:
			sortedblocks = []
			for i in range(0, len(unsortedblocks), memsize):
				sortedblocks.append(externalsort.mergesort(unsortedblocks[i:i + memsize], memsize))
		
		'''Merge the smaller problems together'''
		sortedints = []
		
		# Find the length of the longest block in the list of blocks
		numvalues = 0
		for i in range(len(sortedblocks)):
			numvalues += len(sortedblocks[i])
		
		# Sort by taking the smallest value of the top of each block one at a time
		currmin = (0,)		# (index of smallest value, smallest value)
		for _ in range(numvalues):
			
			currmin = (0, sortedblocks[0][0])
			for currblockindex in range(1, len(sortedblocks)):
				if currmin[1] > sortedblocks[currblockindex][0]:
					currmin = (currblockindex, sortedblocks[currblockindex][0])
		
			# Add the smallest int to the output
			sortedints.append(sortedblocks[currmin[0]].pop(0))
			
			# Remove the block if it has no more values
			if len(sortedblocks[currmin[0]]) == 0:
				sortedblocks.pop(currmin[0])
		
		return sortedints