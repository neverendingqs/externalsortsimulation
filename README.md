Simulation of performing external sorts using mergesort, when there is not enough memory to hold all the values to be sorted at once. Integers are used in this simulation.

This is a stable sort.

@author: neverendingqs